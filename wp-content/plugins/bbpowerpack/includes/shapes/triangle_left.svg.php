<svg class="pp-big-triangle-left" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="currentColor" width="100%" height="<?php echo $height; ?>" viewBox="0 0 2000 90" preserveAspectRatio="none" role="presentation">
	<polygon xmlns="http://www.w3.org/2000/svg" points="535.084,64.886 0,0 0,90 2000,90 2000,0 "></polygon>
</svg>