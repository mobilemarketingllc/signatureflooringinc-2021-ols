<svg class="pp-slit" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="currentColor" width="100%" height="<?php echo $height; ?>" viewBox="0 0 100 100" preserveAspectRatio="none" role="presentation">
	<path class="pp-slit-1" d="M50 100 C49 80 47 0 40 0 L47 0 Z"></path>
	<path class="pp-slit-2" d="M50 100 C51 80 53 0 60 0 L53 0 Z"></path>
	<path class="pp-slit-3" d="M47 0 L50 100 L53 0 Z"></path>
</svg>